﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
	public bool TwoPlayerMode = false;

	public GameObject Region, RegionParent;
	public float ScreenWidth;
	public float StartPositionY;
	public float RegionSpacing;
	public float RegionSize;

	public int Rows, Columns;

	public float Party1Percent;
	public float Party2Percent;
	public float Party3Percent;

	public Text Party1PercentText, Party3PercentText;
	public Text Player1MoneyText, Player2MoneyText;
	public Slider TimeSlider;

	public List<Region> allRegions = new List<Region>();

	public GamepadController gamepad1, gamepad2;
	public int Player1Money, Player2Money;

	public GameObject GameOverObj;
	public Text WinText;

	Vector2 startPosition;
	Vector2 spawnPosition;

	float timeOfLastTick;
	int daysRemaining = 180;

	void Awake ()
	{
		float startX = (-ScreenWidth / 2) + (ScreenWidth - (5 * (RegionSpacing + RegionSize))) / 2;
		startPosition = new Vector2(startX, StartPositionY);
		SpawnRegions();
	}

	void Start ()
	{
		ChangeP1Money(0);
		ChangeP2Money(0);
	}
	
	void Update ()
	{
		GameTick();
	}

	void GameTick()
	{
		if (Time.time >= timeOfLastTick + 1)
		{
			TimeSlider.value = daysRemaining;

			timeOfLastTick = Time.time;

			daysRemaining--;

			if (daysRemaining <= 0)
			{
				EndGame();
			}
		}
	}

	void EndGame()
	{
		if (Party1Percent > Party3Percent)
		{
			WinText.text = "Blue Wins!";
		}
		else if (Party3Percent > Party1Percent)
		{
			WinText.text = "Red Wins!";
			WinText.color = Color.red;
		}

		GameOverObj.SetActive(true);
		Time.timeScale = 0;
	}

	void SpawnRegions()
	{
		int index = 0;
		float newX, newY;
		float regionSizeX = 0, regionSizeY = 0;
		spawnPosition = startPosition;

		for (int i = 0; i < Rows; i++)
		{
			for (int j = 0; j < Columns; j++)
			{
				GameObject newRegion = Instantiate(Region, spawnPosition, Quaternion.identity, RegionParent.transform) as GameObject;

				Region region = newRegion.GetComponent<Region>();
				region.index = index;
				region.gameController = this;
				allRegions.Add(region);

				if (index == 0)
				{
					regionSizeX = newRegion.GetComponent<Collider2D>().bounds.size.x;
					regionSizeY = newRegion.GetComponent<Collider2D>().bounds.size.y;
				}

				newX = spawnPosition.x + RegionSpacing + regionSizeX;
				newY = spawnPosition.y;
				spawnPosition = new Vector2(newX, newY);

				index++;
			}

			newX = startPosition.x;
			newY = spawnPosition.y - RegionSpacing - regionSizeY;
			spawnPosition = new Vector2(newX, newY);
		}

		foreach (Region region in allRegions)
		{
			region.SetNeighbours();
		}
	}

	public void UpdatePartyPercentages()
	{
		float party1 = 0, party2 = 0, party3 = 0;
		foreach (Region region in allRegions)
		{
			party1 += region.Party1Influence;
			party2 += region.Party2Influence;
			party3 += region.Party3Influence;
		}

		float totalInfluence = party1 + party2 + party3;

		Party1Percent = (party1 / totalInfluence) * 100;
		Party2Percent = (party2 / totalInfluence) * 100;
		Party3Percent = (party3 / totalInfluence) * 100;

		Party1PercentText.text = Party1Percent.ToString("F2") + "%";
		//Party1PercentText.text = Party1Percent.ToString("F2") + "%";
		Party3PercentText.text = Party3Percent.ToString("F2") + "%";
	}

	public void ChangeP1Money(int amount)
	{
		Player1Money += amount;
		Player1MoneyText.text = "$" + Player1Money;
	}

	public void ChangeP2Money(int amount)
	{
		Player2Money += amount;
		Player2MoneyText.text = "$" + Player2Money;
	}
}
