﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamepadController : MonoBehaviour
{
	public int Player;
	public float SelectDelay;
	public Region currentlySelectedRegion;

	Region previousRegion;

	GameController gameController;

	float lastTime;

	void Start ()
	{
		gameController = FindObjectOfType<GameController>();

		SetStartRegion();
	}
	
	void Update ()
	{
		if (Time.time > lastTime + SelectDelay)
		{
			if (Input.GetAxis("P" + Player + "Horizontal") > 0.5f)
			{
				if (currentlySelectedRegion.right != null)
				{
					ChangeSelectedRegion(currentlySelectedRegion.right);
				}
				else
				{
					Region temp = currentlySelectedRegion;
					while (temp.left != null)
					{
						temp = temp.left;
					}
					ChangeSelectedRegion(temp);
				}
			}
			else if (Input.GetAxis("P" + Player + "Horizontal") < -0.5f)
			{
				if (currentlySelectedRegion.left != null)
				{
					ChangeSelectedRegion(currentlySelectedRegion.left);
				}
				else
				{
					Region temp = currentlySelectedRegion;
					while (temp.right != null)
					{
						temp = temp.right;
					}
					ChangeSelectedRegion(temp);
				}
			}
			else if (Input.GetAxis("P" + Player + "Vertical") > 0.5f)
			{
				if (currentlySelectedRegion.up != null)
				{
					ChangeSelectedRegion(currentlySelectedRegion.up);
				}
				else
				{
					Region temp = currentlySelectedRegion;
					while (temp.down != null)
					{
						temp = temp.down;
					}
					ChangeSelectedRegion(temp);
				}
			}
			else if (Input.GetAxis("P" + Player + "Vertical") < -0.5f)
			{
				if (currentlySelectedRegion.down != null)
				{
					ChangeSelectedRegion(currentlySelectedRegion.down);
				}
				else
				{
					Region temp = currentlySelectedRegion;
					while (temp.up != null)
					{
						temp = temp.up;
					}
					ChangeSelectedRegion(temp);
				}
			}

			lastTime = Time.time;
		}
	}

	public void SetStartRegion()
	{
		currentlySelectedRegion = gameController.allRegions[0];
		currentlySelectedRegion.StartHover(Player);
	}

	void ChangeSelectedRegion(Region newRegion)
	{
		newRegion.StartHover(Player);
		currentlySelectedRegion.StopHover(Player);
		previousRegion = currentlySelectedRegion;
		currentlySelectedRegion = newRegion;
	}
}
