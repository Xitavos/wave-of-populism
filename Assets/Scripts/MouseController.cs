﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
	Region previousHit;

	void Start ()
	{
		
	}
	
	void Update ()
	{
		RaycastHit2D testHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, 1);

		if (testHit.transform != null && testHit.collider.gameObject.GetComponent<Region>() != null)
		{
			Region hitRegion = testHit.collider.gameObject.GetComponent<Region>();

			if (hitRegion != previousHit)
			{
				hitRegion.StartHover(1);
				if(previousHit != null)
					previousHit.StopHover(1);
			}

			if (Input.GetMouseButtonDown(0))
			{
				hitRegion.ClickedOn();
			}

			previousHit = hitRegion;
		}
		else
		{
			if (previousHit != null)
			{
				previousHit.StopHover(1);
				previousHit = null;
			}
		}
	}
}
