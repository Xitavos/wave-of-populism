﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Region : MonoBehaviour
{
	public int Population;
	//public int EducatedPercent
	public int Wealthiness; //0 = poorest, 10 is richest

	public int MinimumPopulation;
	public int MaximumPopulation;

	[Range(0, 1)] public float Party1Influence;
	[Range(0, 1)] public float Party2Influence;
	[Range(0, 1)] public float Party3Influence;

	[Range(0, 100)]	public float Party1Percent;
	[Range(0, 100)] public float Party2Percent;
	[Range(0, 100)]	public float Party3Percent;

	public bool SpawnPopulationOfCitizens = true; //If false spawns 1

	public GameObject ShowWhenHoveredOver;

	public bool isP1Hover = false, isP2Hover = false;

	public List<Citizen> CitizensInRegion = new List<Citizen>();
	public int index;
	public Region left, right, up, down; //Neighbouring regions

	public GameController gameController;

	public int NewspaperCost, RadioCost, TVCost, InternetCost;
	public float NewspaperEffect, RadioEffect, TVEffect, InternetEffect;
	public float NewspaperCoverage, RadioCoverage, TVCoverage, InternetCoverage;
	public float NewspaperDuration, RadioDuration, TVDuration, InternetDuration;
	public Image P1Icon, P2Icon;
	public GameObject P1IconPanel, P2IconPanel;
	public Sprite NewspaperIcon, RadioIcon, TVIcon, InternetIcon;
	public Image HousingImage1, HousingImage2;
	public Sprite[] PoorHouses;
	public Sprite[] MiddleClassHouses;
	public Sprite[] RichHouses;

	public float MoneyPerInfluenceRate;

	public Transform[] SpawnPoints;

	[SerializeField] Text PopulationText;
	[SerializeField] Text WealthText;

	[SerializeField] GameObject Citizen;
	[SerializeField] Transform citizenParent;

	float P1InfluencePerSecond, P2InfluencePerSecond;
	float P1InfluenceTimeLeft, P2InfluenceTimeLeft;
	float P1Coverage, P2Coverage;

	float lastTickTime;
	
	void Start ()
	{
		CreateRandomPopulation();
		RandomWealthiness();
		if (!SpawnPopulationOfCitizens)
			Population = 2000;
		SpawnCitizens();

		lastTickTime = Time.time;
	}

	void Update()
	{
		if (isP1Hover)
		{
			if (Input.GetAxis("P1A") > 0)
			{
				NewspaperInfluence(1);
			}
			else if(Input.GetAxis("P1B") > 0)
			{
				RadioInfluence(1);
			}
			else if(Input.GetAxis("P1X") > 0)
			{
				TVInfluence(1);
			}
			else if (Input.GetAxis("P1Y") > 0)
			{
				InternetInfluence(1);
			}
		}
		
		if (isP2Hover)
		{
			if (Input.GetAxis("P2A") > 0)
			{
				NewspaperInfluence(2);
			}
			else if (Input.GetAxis("P2B") > 0)
			{
				RadioInfluence(2);
			}
			else if (Input.GetAxis("P2X") > 0)
			{
				TVInfluence(2);
			}
			else if (Input.GetAxis("P2Y") > 0)
			{
				InternetInfluence(2);
			}
		}

		Influence();
	}

	public void SetNeighbours()
	{
		if (index > 0 && gameController.allRegions[index - 1] != null && (index) % gameController.Columns != 0)
			left = gameController.allRegions[index - 1];
		if (index < gameController.allRegions.Count - 1 && gameController.allRegions[index + 1] != null && (index + 1) % gameController.Columns != 0)
			right = gameController.allRegions[index + 1];
		if (index - gameController.Columns >= 0 && gameController.allRegions[index - gameController.Columns] != null)
			up = gameController.allRegions[index - gameController.Columns];
		if (index + gameController.Columns < gameController.allRegions.Count && gameController.allRegions[index + gameController.Columns] != null)
			down = gameController.allRegions[index + gameController.Columns];
	}

	#region oldInput
	/*
	void ApplyInfluence(int player)
	{
		if (Input.GetAxis("P" + player + "A") > 0)
			NewspaperInfluence(player);
		else if (Input.GetAxis("P" + player + "B") > 0)
			RadioInfluence(player);
		else if (Input.GetAxis("P" + player + "X") > 0)
			TVInfluence(player);
		else if (Input.GetAxis("P" + player + "Y") > 0)
			InternetInfluence(player);

		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			foreach (Citizen citizen in CitizensInRegion)
			{
				int i = Random.Range(0, 10);
				if (i <= 5)
				{
					float influenceAmount = Random.Range(0.1f, 0.3f);
					citizen.ChangeParty1Influence(influenceAmount);
					if (!gameController.TwoPlayerMode)
						citizen.ChangeParty2Influence(-influenceAmount / 2);
					citizen.ChangeParty3Influence(-influenceAmount / 2);
				}
			}
		}

		if (Input.GetKeyDown(KeyCode.Alpha2) && !gameController.TwoPlayerMode)
		{
			foreach (Citizen citizen in CitizensInRegion)
			{
				int i = Random.Range(0, 10);
				if (i <= 5)
				{
					float influenceAmount = Random.Range(0.1f, 0.3f);
					citizen.ChangeParty1Influence(-influenceAmount / 2);
					citizen.ChangeParty2Influence(influenceAmount);
					citizen.ChangeParty3Influence(-influenceAmount / 2);
				}
			}
		}

		if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			foreach (Citizen citizen in CitizensInRegion)
			{
				int i = Random.Range(0, 10);
				if (i <= 5)
				{
					float influenceAmount = Random.Range(0.1f, 0.3f);
					citizen.ChangeParty1Influence(-influenceAmount / 2);
					if (!gameController.TwoPlayerMode)
						citizen.ChangeParty2Influence(-influenceAmount / 2);
					citizen.ChangeParty3Influence(influenceAmount);
				}
			}
		}

	}*/

	#endregion

	void Influence()
	{
		if (Time.time >= lastTickTime + 1)
		{
			GiveMoney();

			if (P1InfluenceTimeLeft > 0)
			{
				foreach (Citizen citizen in CitizensInRegion)
				{
					int i = Random.Range(0, 10);
					if (i <= P1Coverage)
					{
						float influenceAmount = P1InfluencePerSecond;
						citizen.ChangeParty1Influence(influenceAmount);
						if (!gameController.TwoPlayerMode)
							citizen.ChangeParty2Influence(-influenceAmount / 2);
						citizen.ChangeParty3Influence(-influenceAmount / 2);
					}
				}

				P1InfluenceTimeLeft -= 1;

				if (P1InfluenceTimeLeft <= 0)
				{
					P1IconPanel.SetActive(false);
				}
			}

			if (P2InfluenceTimeLeft > 0)
			{
				foreach (Citizen citizen in CitizensInRegion)
				{
					int i = Random.Range(0, 10);
					if (i <= P2Coverage)
					{
						float influenceAmount = P2InfluencePerSecond;
						citizen.ChangeParty1Influence(-influenceAmount / 2);
						if (!gameController.TwoPlayerMode)
							citizen.ChangeParty2Influence(-influenceAmount / 2);
						citizen.ChangeParty3Influence(influenceAmount);
					}
				}

				P2InfluenceTimeLeft -= 1;

				if (P2InfluenceTimeLeft <= 0)
				{
					P2IconPanel.SetActive(false);
				}
			}

			lastTickTime = Time.time;
		}
	}

	void GiveMoney()
	{
		int player1Money = Mathf.RoundToInt(Party1Influence * MoneyPerInfluenceRate * Population / 2000);
		int player2Money = Mathf.RoundToInt(Party2Influence * MoneyPerInfluenceRate * Population / 2000);

		gameController.ChangeP1Money(player1Money);
		gameController.ChangeP2Money(player2Money);
	}

	public void NewspaperInfluence(int player)
	{
		if (player == 1 && P1InfluenceTimeLeft <= 0 && gameController.Player1Money >= NewspaperCost)
		{
			P1InfluencePerSecond = NewspaperEffect;
			P1InfluenceTimeLeft = NewspaperDuration;
			P1Coverage = NewspaperCoverage;
			P1Icon.sprite = NewspaperIcon;
			P1IconPanel.SetActive(true);
			gameController.ChangeP1Money(-NewspaperCost);
		}
		else if (player == 2 && P2InfluenceTimeLeft <= 0 && gameController.Player2Money >= NewspaperCost)
		{
			P2InfluencePerSecond = NewspaperEffect;
			P2InfluenceTimeLeft = NewspaperDuration;
			P2Coverage = NewspaperCoverage;
			P2Icon.sprite = NewspaperIcon;
			P2IconPanel.SetActive(true);
			gameController.ChangeP2Money(-NewspaperCost);
		}
	}

	public void RadioInfluence(int player)
	{
		if (player == 1 && P1InfluenceTimeLeft <= 0 && gameController.Player1Money >= RadioCost)
		{
			P1InfluencePerSecond = RadioEffect;
			P1InfluenceTimeLeft = RadioDuration;
			P1Coverage = RadioCoverage;
			P1Icon.sprite = RadioIcon;
			P1IconPanel.SetActive(true);
			gameController.ChangeP1Money(-RadioCost);
		}
		else if (player == 2 && P2InfluenceTimeLeft <= 0 && gameController.Player2Money >= RadioCost)
		{
			P2InfluencePerSecond = RadioEffect;
			P2InfluenceTimeLeft = RadioDuration;
			P2Coverage = RadioCoverage;
			P2Icon.sprite = RadioIcon;
			P2IconPanel.SetActive(true);
			gameController.ChangeP2Money(-RadioCost);
		}
	}

	public void TVInfluence(int player)
	{
		if (player == 1 && P1InfluenceTimeLeft <= 0 && gameController.Player1Money >= TVCost)
		{
			P1InfluencePerSecond = TVEffect;
			P1InfluenceTimeLeft = TVDuration;
			P1Coverage = TVCoverage;
			P1Icon.sprite = TVIcon;
			P1IconPanel.SetActive(true);
			gameController.ChangeP1Money(-TVCost);
		}
		else if (player == 2 && P2InfluenceTimeLeft <= 0 && gameController.Player2Money >= TVCost)
		{
			P2InfluencePerSecond = TVEffect;
			P2InfluenceTimeLeft = TVDuration;
			P2Coverage = TVCoverage;
			P2Icon.sprite = TVIcon;
			P2IconPanel.SetActive(true);
			gameController.ChangeP2Money(-TVCost);
		}
	}

	public void InternetInfluence(int player)
	{
		if (player == 1 && P1InfluenceTimeLeft <= 0 && gameController.Player1Money >= InternetCost)
		{
			P1InfluencePerSecond = InternetEffect;
			P1InfluenceTimeLeft = InternetDuration;
			P1Coverage = InternetCoverage;
			P1Icon.sprite = InternetIcon;
			P1IconPanel.SetActive(true);
			gameController.ChangeP1Money(-InternetCost);
		}
		else if (player == 2 && P2InfluenceTimeLeft <= 0 && gameController.Player2Money >= InternetCost)
		{
			P2InfluencePerSecond = InternetEffect;
			P2InfluenceTimeLeft = InternetDuration;
			P2Coverage = InternetCoverage;
			P2Icon.sprite = InternetIcon;
			P2IconPanel.SetActive(true);
			gameController.ChangeP2Money(-InternetCost);
		}
	}

	public void ClickedOn()
	{
		CreateRandomPopulation();
	}

	void CreateRandomPopulation()
	{
		Population = Random.Range(MinimumPopulation / 100, (MaximumPopulation + 1) / 100) * 100;

		PopulationText.text = "Population: " + Population.ToString();
	}

	void SpawnCitizens()
	{
		for (int i = 0; i < Population / 2000; i++)
		{
			Vector2 position = SpawnPoints[Random.Range(0, 2)].position;
			Vector2 spawnPosition = (Random.insideUnitCircle * 0.5f) + position;
			GameObject newCitizen = Instantiate(Citizen, spawnPosition, Quaternion.identity, citizenParent);

			Citizen citizen = newCitizen.GetComponent<Citizen>();
			citizen.region = this;

			CitizensInRegion.Add(citizen);
		}
	}

	void RandomWealthiness()
	{
		Wealthiness = Random.Range(1, 10);

		string wealthValue = "";
		Sprite house;

		switch (Wealthiness)
		{
			case 1:	case 2:	case 3:
				wealthValue = "Poor";
				HousingImage1.sprite = PoorHouses[Random.Range(0, 2)];
				HousingImage2.sprite = PoorHouses[Random.Range(0, 2)];
				break;
			case 4:	case 5: case 6:
				wealthValue = "Middle Class";
				HousingImage1.sprite = MiddleClassHouses[Random.Range(0, 2)];
				HousingImage2.sprite = MiddleClassHouses[Random.Range(0, 2)];
				break;
			case 7:	case 8:	case 9:
				wealthValue = "Wealthy";
				HousingImage1.sprite = RichHouses[Random.Range(0, 2)];
				HousingImage2.sprite = RichHouses[Random.Range(0, 2)];
				break;
			default:
				break;
		}

		WealthText.text = "Wealthiness: " + wealthValue;
	}

	public void UpdatePartyPercentages()
	{
		float party1 = 0, party2 = 0, party3 = 0;

		foreach (Citizen citizen in CitizensInRegion)
		{
			party1 += citizen.party1Influence;
			party2 += citizen.party2Influence;
			party3 += citizen.party3Influence;
		}

		party1 /= CitizensInRegion.Count;
		party2 /= CitizensInRegion.Count;
		party3 /= CitizensInRegion.Count;

		Party1Influence = party1;
		Party2Influence = party2;
		Party3Influence = party3;

		float totalInfluence = party1 + party2 + party3;

		Party1Percent = (party1 / totalInfluence) * 100;
		Party2Percent = (party2 / totalInfluence) * 100;
		Party3Percent = (party3 / totalInfluence) * 100;

		gameController.UpdatePartyPercentages();
	}

	public void StartHover(int player)
	{
		if (player == 1)
		{
			isP1Hover = true;

		}
		else if (player == 2)
		{
			isP2Hover = true;
		}

		ShowWhenHoveredOver.SetActive(true);
	}

	public void StopHover(int player)
	{
		if (player == 1)
		{
			isP1Hover = false;

		}
		else if (player == 2)
		{
			isP2Hover = false;
		}

		if (!isP1Hover && !isP2Hover)
		{
			ShowWhenHoveredOver.SetActive(false);
		}
	}
}
