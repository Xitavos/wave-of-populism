﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Citizen : MonoBehaviour
{
	[Range(0, 1)] public float party1Influence;
	[Range(0, 1)] public float party2Influence;
	[Range(0, 1)] public float party3Influence;
	public float InfluenceModifier; //How much can this person be influenced

	public bool EnableMinorityPartyPenaliser = false;
	public float MinorityPartyPenaliser;

	public int MajorityParty, MinorityParty;

	public Region region;

	SpriteRenderer spriteRen;
	GameController gameController;

	bool twoPlayer = false;

	void Start ()
	{
		gameController = FindObjectOfType<GameController>();
		twoPlayer = gameController.TwoPlayerMode;

		spriteRen = GetComponent<SpriteRenderer>();
		GenerateRandomPartyAlliegence();
		region.UpdatePartyPercentages();
	}
	
	void Update ()
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y);
	}

	void GenerateRandomPartyAlliegence()
	{
		party1Influence = Random.Range(0f, 1f);
		if(!twoPlayer)
			party2Influence = Random.Range(0f, 1f);
		party3Influence = Random.Range(0f, 1f);

		InfluenceModifier = Random.Range(0.4f, 0.8f);
		UpdateColor();
	}

	void UpdateColor()
	{
		spriteRen.color = GetColor();
		UpdateMajorityMinorityParty();
		region.UpdatePartyPercentages();
	}

	Color GetColor()
	{
		float r, g, b;
		r = percentToColor(party3Influence);
		if (!twoPlayer)
			g = percentToColor(party2Influence);
		else
			g = 0;
		b = percentToColor(party1Influence);

		Color rgbColor = new Color(r, g, b);
		float h, s, v;
		Color.RGBToHSV(rgbColor, out h, out s, out v);
		return Color.HSVToRGB(h, 1.001f - InfluenceModifier, 0.75f);
	}

	float percentToColor(float percent)
	{
		return percent * 255;
	}

	public void ChangeParty1Influence(float changeAmount)
	{
		if (EnableMinorityPartyPenaliser && MinorityParty == 1)
		{
			changeAmount *= MinorityPartyPenaliser;
		}

		changeAmount *= InfluenceModifier;

		party1Influence += changeAmount;

		if (MajorityParty == 1 && changeAmount > 0)
			DecreaseInfluenceMod(changeAmount / 10);

		if (party1Influence > 1)
			party1Influence = 1;
		else if (party1Influence < 0)
			party1Influence = 0;

		UpdateColor();
	}

	public void ChangeParty2Influence(float changeAmount)
	{
		if (!twoPlayer)
		{
			if (EnableMinorityPartyPenaliser && MinorityParty == 2)
			{
				changeAmount *= MinorityPartyPenaliser;
			}

			changeAmount *= InfluenceModifier;

			party2Influence += changeAmount;

			if (MajorityParty == 2 && changeAmount > 0)
				DecreaseInfluenceMod(changeAmount / 10);

			if (party2Influence > 1)
				party2Influence = 1;
			else if (party2Influence < 0)
				party2Influence = 0;

			UpdateColor();
		}
	}

	public void ChangeParty3Influence(float changeAmount)
	{
		if (EnableMinorityPartyPenaliser && MinorityParty == 3)
		{
			changeAmount *= MinorityPartyPenaliser;
		}

		changeAmount *= InfluenceModifier;

		party3Influence += changeAmount;

		if (MajorityParty == 3 && changeAmount > 0)
			DecreaseInfluenceMod(changeAmount / 10);

		if (party3Influence > 1)
			party3Influence = 1;
		else if (party3Influence < 0)
			party3Influence = 0;

		UpdateColor();
	}

	void DecreaseInfluenceMod(float decreaseAmount)
	{
		InfluenceModifier -= decreaseAmount;
		if (InfluenceModifier < 0.1f)
			InfluenceModifier = 0.1f;
	}

	void UpdateMajorityMinorityParty()
	{
		if (party1Influence >= party2Influence && party1Influence >= party3Influence)
			MajorityParty = 1;
		else if (!twoPlayer && party2Influence >= party1Influence && party2Influence >= party3Influence)
			MajorityParty = 2;
		else
			MajorityParty = 3;

		if (party1Influence <= party2Influence && party1Influence <= party3Influence)
			MinorityParty = 1;
		else if (!twoPlayer && party2Influence <= party1Influence && party2Influence <= party3Influence)
			MinorityParty = 2;
		else
			MinorityParty = 3;
	}
}
