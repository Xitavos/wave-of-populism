﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : MonoBehaviour
{
	public Sprite BotLeftAndTopRight, BotRightAndTopLeft;

	Animator animator;
	SpriteRenderer spriteRenderer;
	bool isLerping, isWaiting;
	Vector2 from, to, walkDirection;
	Rigidbody2D myRigidbody;
	float timeStartedLerping;
	float walkTime;

	void Start ()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		animator = GetComponent<Animator>();
		myRigidbody = GetComponent<Rigidbody2D>();
		StartWalking();
	}
	
	void Update ()
	{

	}

	void FixedUpdate()
	{
		if (isLerping && !isWaiting)
		{
			float timeSinceStart = Time.time - timeStartedLerping;
			float percentComplete = timeSinceStart / walkTime;

			myRigidbody.position = Vector2.Lerp(from, to, percentComplete);

			if (percentComplete >= 1.0f)
			{
				isLerping = false;
				isWaiting = true;
			}
		}
		else if (!isLerping && isWaiting)
		{
			isWaiting = false;
			StartCoroutine(waitForNewWalk());
		}
	}

	void StartWalking()
	{
		from = myRigidbody.position;
		walkDirection = newDirection();
		to = from + walkDirection * newWalkDistance();
		timeStartedLerping = Time.time;
		isLerping = true;
		walkTime = newWalkTime();
	}

	float newWalkTime()
	{
		return Random.Range(1.5f, 3f);
	}

	float newWalkDistance()
	{
		return Random.Range(0.3f, 1f);
	}

	Vector2 newDirection()
	{
		int rand = Random.Range(1, 5); //Travel in one of 4 directions: UpLeft, UpRight, DownLeft, DownRight
		Vector2 direction = Vector2.up; // default

		switch (rand)
		{
			case 1:
				direction = Vector2.up + Vector2.left;
				animator.SetInteger("Direction", 3);
				break;
			case 2:
				direction = Vector2.up + Vector2.right;
				animator.SetInteger("Direction", 2);
				break;
			case 3:
				direction = Vector2.down + Vector2.left;
				animator.SetInteger("Direction", 2);
				break;
			case 4:
				direction = Vector2.down + Vector2.right;
				animator.SetInteger("Direction", 3);
				break;
			default:
				break;
		}

		return direction;
	}

	float newWaitTime()
	{
		return Random.Range(1f, 3f);
	}

	IEnumerator waitForNewWalk()
	{
		SetIdle();

		yield return new WaitForSeconds(newWaitTime());

		StartWalking();
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.gameObject.tag == "RegionBorder" && !isWaiting)
		{
			to = myRigidbody.position;
			StartCoroutine(ReverseDirection());
		}
	}

	IEnumerator ReverseDirection()
	{
		isWaiting = true;

		SetIdle();

		yield return new WaitForSeconds(0.5f);

		Vector2 newWalkDirection = newDirection(); //Bounce off wall

		from = myRigidbody.position;
		to = from + newWalkDirection * newWalkDistance();
		walkTime = newWalkTime();

		timeStartedLerping = Time.time;
		isWaiting = false;
	}

	void SetIdle()
	{
		if (animator.GetInteger("Direction") == 2)
		{
			animator.SetInteger("Direction", 0);
		}
		else if (animator.GetInteger("Direction") == 3)
		{
			animator.SetInteger("Direction", 1);
		}
	}
}
